package io.pivotal.pipelinepracticum.atkaair

import io.pivotal.pipelinepracticum.flightsubscriptions.Flight
import io.pivotal.pipelinepracticum.journeys.FlightSubscriptionsJourney
import org.junit.experimental.categories.Category
import java.time.LocalDate

@Category(ProdSmokeTest::class)
class ProdSmokeTest : FlightSubscriptionsJourney() {
    override fun baseUrl() = "https://atka-air-maxillipedary-tameability.apps.pcfone.io/"

    override fun givenSomeFlightsWithStatuses() = mapOf(
            Flight("KS 999", LocalDate.of(2018, 11, 30)) to "Delayed",
            Flight("KS 999", LocalDate.of(2018, 12, 31)) to "On Time"
    )
}