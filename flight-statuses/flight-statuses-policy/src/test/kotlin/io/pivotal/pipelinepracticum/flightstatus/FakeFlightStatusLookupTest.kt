package io.pivotal.pipelinepracticum.flightstatus

class FakeFlightStatusLookupTest: FlightStatusLookupContract() {
    val fakeLookup = FakeFlightStatusLookup()
    override fun lookup() = fakeLookup
    override fun givenAFlightUpdate(update: FlightUpdate) = fakeLookup.add(update)
}