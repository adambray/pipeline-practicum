package io.pivotal.pipelinepracticum.flightstatus

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.time.LocalDate
import java.util.*

abstract class FlightStatusLookupContract {

    abstract fun lookup(): FlightStatusLookup
    abstract fun givenAFlightUpdate(update: FlightUpdate)

    @Test
    fun `getting latest flight updates for different days`() {
        val monday = LocalDate.of(2018, 11, 5)
        val tuesday = LocalDate.of(2018, 11, 6)

        val mondayUpdateFor244 = FlightUpdate("KS ${Random().nextInt(1000000)}", monday, "Got lost")
        val mondayUpdateFor245 = FlightUpdate("KS ${Random().nextInt(1000000)}", monday, "Navigating puffins")
        val tuesdayUpdateFor244 = FlightUpdate("KS ${Random().nextInt(1000000)}", tuesday, "There are so many birds")
        val tuesdayUpdateFor246 = FlightUpdate("KS ${Random().nextInt(1000000)}", tuesday, "Maybe?")

        givenAFlightUpdate(mondayUpdateFor244)
        givenAFlightUpdate(mondayUpdateFor245)
        givenAFlightUpdate(tuesdayUpdateFor244)
        givenAFlightUpdate(tuesdayUpdateFor246)

        assertThat(lookup().updatesFor(monday)).contains(mondayUpdateFor244)
        assertThat(lookup().updatesFor(monday)).contains(mondayUpdateFor245)
        assertThat(lookup().updatesFor(monday)).doesNotContain(tuesdayUpdateFor244)
        assertThat(lookup().updatesFor(monday)).doesNotContain(tuesdayUpdateFor246)
    }

}