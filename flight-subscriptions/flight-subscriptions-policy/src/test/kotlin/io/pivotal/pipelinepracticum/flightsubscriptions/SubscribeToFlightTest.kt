package io.pivotal.pipelinepracticum.flightsubscriptions

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.fail
import org.junit.Test
import java.time.LocalDate
import java.util.*

class SubscribeToFlightTest {

    private val passengerRepository: PassengerRepository = FakePassengerRepository()
    private val subscribeToFlight = SubscribeToFlight(passengerRepository)

    @Test
    fun `subscribing to a flight with a known passenger id`() {
        val passengerId = givenAPassenger()

        subscribeToFlight.execute(
                "KS 244",
                LocalDate.of(2018, 11, 5),
                passengerId,

                object : DummyOutcome() {
                    override fun successfullySubscribed(updatedPassenger: Passenger) {
                        assertThat(updatedPassenger.subscriptions.map { it.flightNumber }).contains("KS 244")
                        assertThat(
                                updatedPassenger.subscriptions.find { it.flightNumber == "KS 244" }!!.date
                        ).isEqualTo(
                                LocalDate.of(2018, 11, 5)
                        )
                    }
                }
        )

        assertThat(passengerRepository.findPassengerById(passengerId)!!.subscriptions.map { it.flightNumber }).contains("KS 244")
    }

    @Test
    fun `subscribing to a flight with an unknown passenger id`() {
        subscribeToFlight.execute("KS 244", LocalDate.now(), "nonsense-passenger-id", object : DummyOutcome() {
            override fun noSuchPassenger(passengerId: String) {
                assertThat(passengerId).isEqualTo("nonsense-passenger-id")
            }
        })
    }

    private fun givenAPassenger(): String {
        return passengerRepository.savePassenger(Passenger("Test User", UUID.randomUUID().toString())).id!!
    }

    open class DummyOutcome : SubscribeToFlight.Outcome<Unit> {
        override fun successfullySubscribed(updatedPassenger: Passenger) {
            fail<Unit>("Should not have invoked successfullySubscribed outcome")
        }

        override fun noSuchPassenger(passengerId: String) {
            fail<Unit>("Should not have invoked noSuchPassenger outcome")
        }
    }
}