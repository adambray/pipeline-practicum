package io.pivotal.pipelinepracticum.flightsubscriptions.web

import io.pivotal.pipelinepracticum.flightsubscriptions.*
import io.pivotal.pipelinepracticum.flightsubscriptions.web.FlightSubscriptionsWebTestConfig.Companion.flightsWithStatuses
import io.pivotal.pipelinepracticum.journeys.FlightSubscriptionsJourney
import org.fluentlenium.adapter.junit.FluentTest
import org.junit.runner.RunWith
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import java.time.LocalDate

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [FlightSubscriptionsWebTestConfig::class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("FlightSubscriptionsWebTest")
class FlightSubscriptionsWebTest : FlightSubscriptionsJourney() {

    @LocalServerPort
    private lateinit var port: String

    override fun baseUrl() = "http://localhost:${port}"

    override fun givenSomeFlightsWithStatuses() = flightsWithStatuses

}

@SpringBootApplication
@Profile("FlightSubscriptionsWebTest")
class FlightSubscriptionsWebTestConfig {

    companion object {
        val flightsWithStatuses = mapOf(
                Flight("KS 244", LocalDate.of(2018, 11, 5)) to "Delayed",
                Flight("KS 244", LocalDate.of(2018, 11, 12)) to "On Time"
        )
    }

    @Bean
    fun passengerRepository() = FakePassengerRepository()

    @Bean
    fun subscribeToFlight(passengerRepository: PassengerRepository) = SubscribeToFlight(passengerRepository)

    @Bean
    fun subscribedFlightStatuses(): SubscribedFlightStatuses = object : SubscribedFlightStatuses {
        override fun forPassenger(passenger: Passenger): Map<Flight, String> = flightsWithStatuses
    }
}