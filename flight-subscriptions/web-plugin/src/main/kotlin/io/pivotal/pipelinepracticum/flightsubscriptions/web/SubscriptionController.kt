package io.pivotal.pipelinepracticum.flightsubscriptions.web

import io.pivotal.pipelinepracticum.flightsubscriptions.Passenger
import io.pivotal.pipelinepracticum.flightsubscriptions.PassengerRepository
import io.pivotal.pipelinepracticum.flightsubscriptions.SubscribeToFlight
import io.pivotal.pipelinepracticum.flightsubscriptions.SubscribedFlightStatuses
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import java.time.LocalDate

@Controller
class SubscriptionController(
        val passengerRepository: PassengerRepository,
        val subscribeToFlight: SubscribeToFlight,
        val subscribedFlightStatuses: SubscribedFlightStatuses
) {

    @GetMapping("/flight-subscription")
    fun newSubscription(model: Model): String {
        model.addAttribute("passengers", passengerRepository.findAllPassengers())
        return "new-subscription"
    }

    @PostMapping("/flight-subscription")
    fun createSubscription(

            @RequestParam("passenger_id") passengerId: String,
            @RequestParam("flight_number") flightNumber: String,
            @RequestParam("date") date: String

    ) = subscribeToFlight.execute(flightNumber, LocalDate.parse(date), passengerId, object : SubscribeToFlight.Outcome<String> {
        override fun successfullySubscribed(updatedPassenger: Passenger) = "index"
        override fun noSuchPassenger(passengerId: String) = "error"
    })

    @GetMapping("/flight-subscriptions")
    fun flightSubscriptionsIndex(model: Model): String {
        model.addAttribute("passengers", passengerRepository.findAllPassengers())
        return "flight-subscriptions-passenger-select"
    }

    @GetMapping("/flight-subscriptions/{passengerId}")
    fun flightSubscriptionsForPassenger(@PathVariable("passengerId") passengerId: String, model: Model): String {
        val passenger = passengerRepository.findPassengerById(passengerId)!!
        val updates = subscribedFlightStatuses.forPassenger(passenger).entries

        model.addAttribute("updates", updates)
        model.addAttribute("passengerName", passenger.name)

        return "flight-subscriptions-for-passenger"
    }
}