package io.pivotal.pipelinepracticum.flightsubscriptions.db

import java.io.Serializable
import java.sql.Date
import javax.persistence.*

@Entity
@Table(name = "flight_subscription")
@IdClass(FlightSubscriptionKey::class)
data class FlightSubscriptionJpaEntity(
        @Id val flightNumber: String,
        @Id val flightDate: Date,

        @ManyToOne(fetch= FetchType.LAZY)
        @JoinColumn(name="passenger_id")
        val passenger: PassengerJpaEntity
)

data class FlightSubscriptionKey(
        val flightNumber: String? = null,
        val flightDate: Date? = null
): Serializable