#!/bin/sh
set -e

cd git
./gradlew build
cp atka-air-webapp/build/libs/atka-air-webapp-0.0.1-SNAPSHOT.jar ../build/atka-air.jar