package io.pivotal.pipelinepracticum.journeys

import io.pivotal.pipelinepracticum.flightsubscriptions.Flight
import org.assertj.core.api.Assertions
import org.fluentlenium.adapter.junit.FluentTest
import org.fluentlenium.core.action.Fill
import org.fluentlenium.core.filter.FilterConstructor
import org.junit.Test
import org.junit.experimental.categories.Category
import java.util.*

@Category(AbstractJourney::class)
abstract class FlightSubscriptionsJourney : FluentTest() {

    protected abstract fun baseUrl(): String
    protected abstract fun givenSomeFlightsWithStatuses(): Map<Flight, String>

    @Test
    fun `signing up and subscribing to flight updates`() {
        goTo(baseUrl())

        val testPassengerName = "Test Passenger ${Random().nextInt(1000000)}"

        createPassenger(testPassengerName)

        givenSomeFlightsWithStatuses().forEach { flight, _ ->
            subscribeToFlight(testPassengerName, flight.flightNumber, flight.date.toString())
        }

        viewFlightsFor(testPassengerName)

        givenSomeFlightsWithStatuses().forEach { flight, status ->
            listOfFlightsShouldInclude("${flight.flightNumber}, ${flight.date}: $status")
        }
    }

    private fun listOfFlightsShouldInclude(flightInfo: String) {
        Assertions.assertThat(`$`("li").map { it.text() }).contains(flightInfo)
    }

    private fun viewFlightsFor(passengerName: String) {
        clickLink("View Flight Updates")
        clickLink(passengerName)
    }

    private fun subscribeToFlight(passengerName: String, flightNumber: String, date: String) {
        clickLink("Subscribe to Flight")
        chooseOptionFor("Passenger Name", passengerName)
        fillInputLabeled("Flight Number").with(flightNumber)
        fillInputLabeled("Date").with(date)
        submitForm()
    }

    private fun createPassenger(name: String) {
        clickLink("Sign up")
        fillInputLabeled("Name").with(name)
        submitForm()
    }

    private fun submitForm() {
        `$`("button", FilterConstructor.withText("Submit")).submit()
    }

    private fun clickLink(linkText: String) {
        `$`("a", FilterConstructor.withText(linkText)).click()
    }

    private fun fillInputLabeled(labelText: String): Fill<*> {
        val matchingLabels = `$`("label", FilterConstructor.withText(labelText))
        if(matchingLabels.size == 0) throw RuntimeException("Couldn't find a label with text $labelText")

        val label = matchingLabels[0]

        val inputId = label.attribute("for")
        val matchingInputs = `$`("input", FilterConstructor.withId(inputId))
        if(matchingInputs.size == 0) throw RuntimeException("Couldn't find an input associated with label $labelText. The label's for attribute referenced id '$inputId'")
        val input = matchingInputs[0]

        return input.fill()
    }

    private fun chooseOptionFor(labelText: String, optionText: String) {
        val matchingLabels = `$`("label", FilterConstructor.withText(labelText))
        if(matchingLabels.size == 0) throw RuntimeException("Couldn't find a label with text $labelText")

        val label = matchingLabels[0]

        val selectId = label.attribute("for")
        val matchingSelects = `$`("select", FilterConstructor.withId(selectId))
        if(matchingSelects.size == 0) throw RuntimeException("Couldn't find a select associated with label $labelText. The label's for attribute referenced id '$selectId'")
        val select = matchingSelects[0]

        val matchingOptions = select.find("option", FilterConstructor.withText(optionText))
        if(matchingOptions.size == 0) throw RuntimeException("Couldn't find an option with text '$optionText' in the select element labeled '$labelText'.")

        matchingOptions.click()
    }
}